## The Pizza Tusk

This repository contains the admin backend website for The Pizza Tusk, a fictional pizza parlor made by Rodrigo Lanas for The Pizza Task.

This is a Laravel 6 app, please refer to their website for requirements and install instructions (basically put it in your php enabled web server, npm install, and configure the route/domain/subdomain you want to use).

This backend has a login (User: rodrigo@contraculto.com Password: animals123) and lets you create, edit and delete products.

There is also a route (api/products) that returns a JSON object with the products, this is consumed by the frontend site.

The database is a MySQL hosted with a remote service, please refer to the config for those details should you want to check that.