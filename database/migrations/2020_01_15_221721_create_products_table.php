<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('type');
            $table->string('description');
            $table->float('price_usd');
            $table->string('picture');
            $table->string('vegetarian');
            $table->timestamps();
        });

        DB::table('products')->insert(['id' => '1', 'type' => 'pizza', 'name' => 'Plain Queso', 'description' => 'Just sauce and our delicious mozzarella chesee, no extras needed.', 'price_usd' => '8', 'picture' => 'pizza-pepperoni.jpg', 'vegetarian' => 'yes' ]);
        DB::table('products')->insert(['id' => '2', 'type' => 'pizza', 'name' => 'Pepperoni', 'description' => 'Sauce, mozzarella, pepperoni. This is the way.', 'price_usd' => '9', 'picture' => 'pizza.jpg', 'vegetarian' => 'no' ]);
        DB::table('products')->insert(['id' => '3', 'type' => 'pizza', 'name' => 'Hawaiian', 'description' => 'Sauce, mozzarella, ham, pineapple. Not sure where this one comes from.', 'price_usd' => '9', 'picture' => 'pizza-pepperoni.jpg', 'vegetarian' => 'no' ]);
        DB::table('products')->insert(['id' => '4', 'type' => 'pizza', 'name' => 'Global Warming', 'description' => 'White sauce, mozzarella, shrimp, green olives. Goes very good with a glass of white wine.', 'price_usd' => '9', 'picture' => 'pizza.jpg', 'vegetarian' => 'no' ]);
        DB::table('products')->insert(['id' => '5', 'type' => 'pizza', 'name' => 'Aubergenious', 'description' => 'Mozzarella, Eggplant, garlic, oregano, olive oil. Surely it\'s typical of somewhere.', 'price_usd' => '9', 'picture' => '/pizza2.jpg', 'vegetarian' => 'yes' ]);
        DB::table('products')->insert(['id' => '6', 'type' => 'pizza', 'name' => 'The Pizza Tusk', 'description' => 'Sauce, mozarella, pepperoni, pineapple, more mozarella. 10/10 Good boy.', 'price_usd' => '9', 'picture' => '/pizza-pepperoni.jpg', 'vegetarian' => 'no' ]);
        DB::table('products')->insert(['id' => '7', 'type' => 'pizza', 'name' => 'Mary Ingalls', 'description' => 'Sauce, mozzarella, sun-dried tomatoes, goat cheese, arugula. It\'s the simple things.', 'price_usd' => '9', 'picture' => 'pizza.jpg', 'vegetarian' => 'yes' ]);
        DB::table('products')->insert(['id' => '8', 'type' => 'pizza', 'name' => 'All Out', 'description' => 'Sauce, mozzarella, bacon, onion, green olives, red peppers, jalapeño peppers. Not really all out but still.', 'price_usd' => '10', 'picture' => 'pizza2.jpg', 'vegetarian' => 'no' ]);
        DB::table('products')->insert(['id' => '9', 'type' => 'side', 'name' => 'French Fries', 'description' => 'Of course we have french fries.', 'price_usd' => '4', 'picture' => 'french-fries.jpg', 'vegetarian' => 'yes' ]);
        DB::table('products')->insert(['id' => '10', 'type' => 'side', 'name' => 'Mixed Salad', 'description' => 'Lettuce, tomatoes, red onion, celery, green beans.', 'price_usd' => '4', 'picture' => 'salad.jpg', 'vegetarian' => 'yes' ]);
        DB::table('products')->insert(['id' => '11', 'type' => 'side', 'name' => 'Minced Meat', 'description' => 'Fried. It\'s your life, do as you please.', 'price_usd' => '5', 'picture' => 'meat.jpg', 'vegetarian' => 'no' ]);
        DB::table('products')->insert(['id' => '12', 'type' => 'side', 'name' => 'Vanilla Ice Cream', 'description' => 'Yeah, it\'s actually a dessert. Sue us.', 'price_usd' => '4', 'picture' => 'ice-cream.jpg', 'vegetarian' => 'yes' ]);
        DB::table('products')->insert(['id' => '13', 'type' => 'drink', 'name' => 'Sparkling Water', 'description' => 'So refreshing you wouldn\'t believe it.', 'price_usd' => '2', 'picture' => 'water.jpg', 'vegetarian' => 'no' ]);
        DB::table('products')->insert(['id' => '14', 'type' => 'drink', 'name' => 'Coca - Cola', 'description' => 'A classic drink made of love and sugar.', 'price_usd' => '3', 'picture' => 'coca-cola.jpg', 'vegetarian' => 'no' ]);
        DB::table('products')->insert(['id' => '15', 'type' => 'drink', 'name' => 'Pepsi Light', 'description' => 'The other other cold drink, enjoy eith ice.', 'price_usd' => '3', 'picture' => 'pepsi-light.jpg', 'vegetarian' => 'no' ]);
        DB::table('products')->insert(['id' => '16', 'type' => 'drink', 'name' => 'White Wine', 'description' => 'Goes great with the seafood pizza, also very pretty.', 'price_usd' => '4', 'picture' => 'white-wine.jpg', 'vegetarian' => 'no' ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
