@extends('layouts.app')
@section('title', 'Edit Product')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h1>Edit product</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-8">
			<form action="{{route('home/products.update')}}" method = "post">
				@csrf
				<div class="form-group">
					<label for="name">Name:</label>
					<input type="text" name = "name" id = "name" class="form-control" required value = "{{$product->name}}">
				</div>
				<div class="form-group">
					<label for="type">Type:</label>
					<select name = "type" id = "type" class="form-control" required value = "{{$product->type}}">
						@if ($product->type == 'pizza')
							<option value="pizza" selected>Pizza</option>
						@else
							<option value="pizza">Pizza</option>
						@endif
						@if ($product->type == 'side')
							<option value="side" selected>Side</option>
						@else
							<option value="side">Sside</option>
						@endif
						@if ($product->type == 'drink')
							<option value="drink" selected>Drink</option>
						@else
							<option value="drink">Drink</option>
						@endif
					</select>
				</div>
				<div class="form-group">
					<label for="description">Description:</label>
					<input type="text" name = "description" id = "description" class="form-control" required value = "{{$product->description}}">
				</div>
				<div class="form-group">
					<label for="price_usd">Price (USD):</label>
					<input type="text" name = "price_usd" id = "price_usd" class="form-control" required value = "{{$product->price_usd}}">
				</div>
				<div class="form-group">
					<label for="picture">Picture:</label>
					<input type="text" name = "picture" id = "picture" class="form-control" required value = "{{$product->picture}}">
				</div>
				<div class="form-group">
					<label for="vegetarian">Vegetarian:</label>
					<select name = "vegetarian" id = "vegetarian" class="form-control" required>
						@if ($product->vegetarian == 'yes')
							<option value="yes" selected>Yes</option>
						@else
							<option value="yes">Yes</option>
						@endif
						@if ($product->vegetarian == 'no')
							<option value="no" selected>No</option>
						@else
							<option value="no">No</option>
						@endif
					</select>
				</div>
				<input type="hidden" name="id" value = "{{$product->id}}">
				<button type = "submit" class = "btn btn-success">Submit</button>
			</form>
		</div>
	</div>
</div>
@endsection