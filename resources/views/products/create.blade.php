@extends('layouts.app')
@section('title', 'Create Product')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h1>Create product</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-8">
			<form action="{{route('home/products.store')}}" method = "post">
			@csrf
				<div class="form-group">
					<label for="name">Name:</label>
					<input type="text" name = "name" id = "name" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="type">Type:</label>
					<select name = "type" id = "type" class="form-control" required>
						<option value="pizza">Pizza</option>
						<option value="side">Side</option>
						<option value="drink">Drink</option>
					</select>
				</div>
				<div class="form-group">
					<label for="description">Description:</label>
					<input type="text" name = "description" id = "description" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="price_usd">Price (USD):</label>
					<input type="text" name = "price_usd" id = "price_usd" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="picture">Picture:</label>
					<input type="text" name = "picture" id = "picture" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="vegetarian">Vegetarian:</label>
					<select name = "vegetarian" id = "vegetarian" class="form-control" required>
						<option value="yes">Yes</option>
						<option value="no">No</option>
					</select>
				</div>
				<button type = "submit" class = "btn btn-success">Submit</button>
			</form>
		</div>
	</div>
</div>
@endsection