@extends('layouts.app')
@section('title', 'Products Index')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h1>Products <a href="{{route('home/products.create')}}" class = "btn btn-success btn-create">Create product</a></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<table class="table">
				<tr>
					<th>ID</th>
					<th>Type</th>
					<th>Name</th>
					<th>Description</th>
					<th>Price</th>
					<th></th>
					<th></th>
				</tr>
				@foreach($products as $product)
					<tr>
						<td>{{ $product->id }}</td>
						<td>{{ $product->type }}</td>
						<td>{{ $product->name }}</td>
						<td>{{ $product->description }}</td>
						<td>{{ $product->price_usd }}</td>
						<td><a href="{{route('home/products.edit',['id'=>$product->id])}}" class = "btn btn-info">Edit</a></td>
						<td><a href="{{route('home/products.destroy',['id'=>$product->id])}}" class = "btn btn-danger">Delete</a></td>
					</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>
@endsection