<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Show all products from the database and return to view
        $products = Product::all();
        return view('products.index', ['products'=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Return view to create product
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Persist the product in the database
        //form data is available in the request object
        $product = new Product();
        //input method is used to get the value of input with its
        //name specified
        $product->name = $request->input('name');
        $product->type = $request->input('type');
        $product->description = $request->input('description');
        $product->price_usd = $request->input('price_usd');
        $product->picture = $request->input('picture');
        $product->vegetarian = $request->input('vegetarian');
        $product->save(); //persist the data
        return redirect()->route('home/products.index')->with('info','Product Added Successfully');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Find the product
        $product = Product::find($id);
        return view('products.edit', ['product'=> $product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //Retrieve the product and update
        $product = Product::find($request->input('id'));

        $product->name = $request->input('name');
        $product->type = $request->input('type');
        $product->description = $request->input('description');
        $product->price_usd = $request->input('price_usd');
        $product->picture = $request->input('picture');
        $product->vegetarian = $request->input('vegetarian');
        $product->save(); //persist the data
        return redirect()->route('home/products.index')->with('info', 'Product Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Retrieve the product
        $product = Product::find($id);
        //delete
        $product->delete();
        return redirect()->route('home/products.index');
    }
}