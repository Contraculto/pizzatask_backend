<?php

use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//  No frontpage, we redirect to admin panel or login
Route::get('/', function () {
    if ( Auth::check() ) {
        return redirect('/home');
    } else {
        return redirect("/login");
    }
});

//  Authentication
Auth::routes(['register' => false]);

//  Home for logged in users
Route::get('/home', 'HomeController@index')->name('home');

//  Products
Route::get('/home/products', 'ProductController@index')->name('home/products.index');
Route::get('/home/products/{id}/edit','ProductController@edit')->name('home/products.edit');
Route::get('/home/products/{id}/delete','ProductController@destroy')->name('home/products.destroy');
Route::get('/home/products/create','ProductController@create')->name('home/products.create');
Route::post('/home/products/create','ProductController@store')->name('home/products.store');
Route::post('/home/products/update','ProductController@update')->name('home/products.update');